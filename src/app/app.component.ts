import { Component, AfterViewInit, ViewChild, Inject } from '@angular/core';
import {
  MatTableDataSource,
  MatSort,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
  MatChipInputEvent,
  MatOption,
  MatAutocompleteSelectedEvent,
  MatAutocompleteTrigger,
  MatCheckboxChange
} from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { print } from 'util';
/**
 * @title with sections
 */
@Component({
  selector: 'app-list-sections-example',
  styleUrls: ['app.component.css'],
  templateUrl: 'app.component.html'
})
export class TableFilteringExampleComponent implements AfterViewInit {
  taskList: Task[];
  TaskDao: TaskDAO | null;
  displayedColumns = ['completed', 'title'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatSort)
  sort: MatSort;

  constructor(private http: HttpClient, public dialog: MatDialog) {}
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  taskCompleted(ev: MatCheckboxChange, task: Task): void {
    task.completed = ev.checked;
  }
  ngAfterViewInit() {
    this.taskList = Tasks;
    this.dataSource.data = this.taskList;
    this.dataSource.sort = this.sort;
  }
  openNewDialog(): void {
    const dialogRef = this.dialog.open(AddEditTaskDialogComponent, {
      width: '600px',
      data: {
        task: {
          id: 1,
          title: '',
          description: '',
          completed: false,
          priority: 3,
          due_date: new Date(),
          subtasks: [{ id: 1, title: 'Test subTask', completed: false }],
          tags: []
        },
        insert: false
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.insert === true) {
        console.log(result);
        this.taskList.push(result.task);
        this.dataSource = new MatTableDataSource(this.taskList);
        this.dataSource.sort = this.sort;
      }
    });
  }
  openDialog(currentTask?: Task): void {
    if (currentTask === null) {
      currentTask = {
        id: null,
        title: '',
        description: '',
        completed: false,
        priority: 3,
        due_date: new Date(),
        subtasks: [{ id: 1, title: 'Test subTask', completed: false }],
        tags: []
      };
    }
    const dialogRef = this.dialog.open(AddEditTaskDialogComponent, {
      width: '600px',
      data: {
        task: currentTask,
        insert: false
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }
}

export interface SubTask {
  id: number;
  title: string;
  completed: boolean;
}
export interface Tag {
  title: string;
  color: string;
}

export interface Task {
  id: number | null;
  title: string;
  completed: boolean;
  priority: number | null;
  description: string | null;
  due_date: Date | null;
  subtasks: SubTask[] | null;
  tags: Tag[] | null;
}
const Tasks: Task[] = [
  {
    id: 1,
    title: 'Test1',
    description: 'This is a test description',
    completed: false,
    priority: 3,
    due_date: new Date(),
    subtasks: [{ id: 1, title: 'Test subTask', completed: false }],
    tags: [{ title: 'Test Tag', color: '123456' }]
  },
  {
    id: 2,
    title: 'Test2',
    description: 'This is a test description',
    completed: false,
    priority: 3,
    due_date: new Date(),
    subtasks: [{ id: 2, title: 'Test subTask', completed: false }],
    tags: [{ title: 'Test Tag', color: '123456' }]
  },
  {
    id: 3,
    title: 'Test3',
    description: 'This is a test description',
    completed: false,
    priority: 3,
    due_date: new Date(),
    subtasks: [{ id: 3, title: 'Test subTask', completed: false }],
    tags: [{ title: 'Test Tag', color: '123456' }]
  },
  {
    id: 4,
    title: 'Test4',
    description: 'This is a test description',
    completed: false,
    priority: 3,
    due_date: new Date(),
    subtasks: [{ id: 4, title: 'Test subTask', completed: false }],
    tags: [{ title: 'Test Tag', color: '123456' }]
  }
];
const TAGS: Tag[] = [
  { title: 'Free Time', color: '123456' },
  { title: 'Personal', color: '12ffff' }
];
export class TaskDAO {
  constructor(private http: HttpClient) {}

  getTaskList(): Observable<Task[]> {
    return this.http.get<Task[]>('http://localhost:5000/_todo');
  }
}

@Component({
  selector: 'app-add-edit-task-dialog',
  templateUrl: 'add-edit-task-dialog.html'
})
export class AddEditTaskDialogComponent {
  tagsCtrl: FormControl;
  filteredTags: Observable<any[]>;
  tags: Tag[] = [];
  tagsList: Tag[] = TAGS;
  task: Task;
  separatorKeysCodes = [ENTER];
  removable: Boolean = true;
  subtasks: SubTask[] = [{ id: 3, title: '', completed: false }];

  constructor(
    public dialogRef: MatDialogRef<AddEditTaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.tagsCtrl = new FormControl();
    this.filteredTags = this.tagsCtrl.valueChanges.pipe(
      startWith(''),
      map(tag => (tag ? this.findTags(tag) : this.tags.slice()))
    );
    this.task = data.task;
    this.subtasks = this.task.subtasks;
    this.tags = this.task.tags;
  }
  onOkClick(): void {
    this.dialogRef.close({ task: this.task, insert: true });
  }
  remove(tag): void {
    const idx = this.tags.indexOf(tag);
    if (idx >= 0) {
      this.tags.splice(idx, 1);
    }
  }
  selected(ev: MatAutocompleteSelectedEvent): void {
    const val: Tag = ev.option.value;
    if (this.tags.indexOf(val) === -1) {
      this.tags.push(val);
    }
  }
  add(ev: MatChipInputEvent) {
    if ((ev.value || '').trim()){
      this.tags.push({'title':ev.value.trim(), 'color': '000000'});
    }
    if (ev.input) {
      ev.input.value = '';
    }
    this.tagsCtrl.setValue(null);
  }
  findTags(title: string | Tag) {
    if (typeof title === 'string') {
      return this.tagsList.filter(
        tag => tag.title.toLowerCase().indexOf(title.toLowerCase()) === 0
      );
    } else {
      return this.tagsList.filter(
        tag => tag.title.toLowerCase().indexOf(title.title.toLowerCase()) === 0
      );
    }
  }
  add_subtask(stask) {
    if (stask.title !== '') {
      if (
        this.subtasks[this.subtasks.length - 1].title !== '' ||
        this.subtasks.length <= 1
      ) {
        this.subtasks.push({ id: 3, title: '', completed: false });
      }
    } else {
      this.subtasks.splice(this.subtasks.indexOf(stask), 1);
    }
  }
  closeDialog() {
    this.dialogRef.close({ task: this.task, insert: false });
  }
}
